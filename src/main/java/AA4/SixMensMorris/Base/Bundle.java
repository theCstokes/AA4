package AA4.SixMensMorris.Base;

import java.util.HashMap;
import java.util.Map;

public class Bundle {
	private Map<String, Object> data;
	
	public Bundle() {
		data = new HashMap<>();
	}
	
	public void store(String name, Object obj) {
		this.data.put(name, obj);
	}
	
	public Object get(String name) {
		return data.get(name);
	}
}
