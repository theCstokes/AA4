package AA4.SixMensMorris.Base;

import java.lang.reflect.Field;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import AA4.SixMensMorris.App;
import AA4.SixMensMorris.Game.View.Board;
import AA4.SixMensMorris.Game.View.BoardLocation;
import AA4.SixMensMorris.Game.View.CircleButton;
import AA4.SixMensMorris.Game.View.ItemSelecter;
import AA4.SixMensMorris.SetupPage.Data.User;

public abstract class Controller<T extends Composite> {
	private final String SPLIT_STRING = ".";

	public T view;

	public void bind(String actionString, Control ctrl) {

		if (ctrl.getClass().equals(Label.class)) {
			bind(actionString, (Label) ctrl);
		} else if (ctrl.getClass().equals(Text.class)) {
			bind(actionString, (Text) ctrl);
		} else if (ctrl.getClass().equals(ItemSelecter.class)) {
			bind(actionString, (ItemSelecter) ctrl);
		} else if (ctrl.getClass().equals(Board.class)) {
			bind(actionString, (Board) ctrl);
		} else if(ctrl.getClass().equals(Button.class)) {
			bind(actionString, (Button) ctrl);
		}
	}

	private <T extends NotifyPropertyChanged> void bind(String propertyName, Label text) {
		String pName = getPropertyName(propertyName);
		getDataModel(propertyName).addSubscriber((String) pName, e -> {
			text.setText((String) e.getNewValue());
		});
	}

	private <T extends NotifyPropertyChanged> void bind(String methodString, Text text) {
		T npc = getDataModel(methodString);
		String propertyName = getPropertyName(methodString);

		text.addListener(SWT.Modify, e -> {
			ControllerUtils.set(npc, propertyName, text.getText());
		});
	}

	private <T extends NotifyPropertyChanged> void bind(String methodString, ItemSelecter selecter) {
		T npc = getDataModel(methodString);
		String propertyName = getPropertyName(methodString);

		selecter.addListener(SWT.MouseDown, e -> {
			if (e.widget instanceof CircleButton) {
				CircleButton cb = (CircleButton) e.widget;
				ControllerUtils.set(npc, propertyName, cb.getId());
			}
		});
	}

	private <T extends NotifyPropertyChanged> void bind(String methodString, Board board) {
		T npc = getDataModel(methodString);
		String propertyName = getPropertyName(methodString);

		board.addPeiceListener(SWT.MouseDown, e -> {
			if (e.data instanceof BoardLocation) {
				BoardLocation loc = (BoardLocation) e.data;
				User activeUser = (User) ControllerUtils.get(npc, propertyName);
				loc.setColor(activeUser.getColor().getRGB());
				App.gameLogic.addElementBoardState(e.x, e.y, activeUser.getColor().getName());
			}
		});

	}
	
	private <T extends NotifyPropertyChanged> void bind(String commandName, Button btn) {
		AbstractCommand command = getCommand(commandName);
		command.addSubscriber(commandName, e -> {
			btn.setEnabled((Boolean) e.getNewValue());
        });
		btn.addListener(SWT.MouseDown, e -> {
            if(command.canExecute()) {
                command.execute();
            }
        });

	}

	// private void bind(String commandName, Button button) {
	// AbstractCommand command = getCommand(commandName);
	// command.addSubscriber(commandName, e -> {
	// button.setEnabled((Boolean) e.getNewValue());
	// });
	// button.setOnClickListener(e -> {
	// if(command.canExecute()) {
	// command.execute();
	// }
	// });
	// }

	private <T extends NotifyPropertyChanged> T getDataModel(String propertyString) {
		String[] parts = propertyString.split("\\"+SPLIT_STRING, 2);
		try {
			Field f = this.getClass().getDeclaredField(parts[0]);
			f.setAccessible(true);
			if (f.getType() == NotifyPropertyChanged.class
					|| f.getType().getSuperclass() == NotifyPropertyChanged.class) {
				return (T) f.get(this);
			}
		} catch (NoSuchFieldException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
		return null;
	}

	private <T extends NotifyPropertyChanged> T getCommand(String propertyString) {
		try {
			Field f = this.getClass().getDeclaredField(propertyString);
			f.setAccessible(true);
			Object o = f.get(this);
			if(o instanceof NotifyPropertyChanged) {
				return (T) o;
			}
		} catch (NoSuchFieldException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
		return null;
	}

	private String getPropertyName(String propertyString) {
		String[] parts = propertyString.split("\\"+SPLIT_STRING, 2);
		return parts[1];
	}

	public <Q extends Composite> void setContentView(T view) {
		this.view = view;
	}

	public abstract void onCreate();

}
