package AA4.SixMensMorris.Base;

import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Subscriptions {
    private Map<String, List<PropertyChangeListener>> subscribers;

    public Subscriptions() {
        subscribers = new HashMap<>();
    }

    public List<PropertyChangeListener> getSubscribers(String name) {
        return subscribers.get(name);
    }

    public void addSubscriber(String name, PropertyChangeListener l) {
        if(subscribers.containsKey(name)) {
            subscribers.get(name).add(l);
        } else {
            subscribers.put(name, new ArrayList<PropertyChangeListener>(Arrays.asList(l)));
        }
    }
}
