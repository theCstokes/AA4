package AA4.SixMensMorris.Base;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class NotifyPropertyChanged extends DataErrorProvider{
	Subscriptions subscribers = new Subscriptions();
    Map<String, Object> dataStore = new HashMap<>();

    public void raisePropertyChanged(String property) {
        List<PropertyChangeListener> s = subscribers.getSubscribers(property);
        if(s != null) {
            Object newData = property.equals("error") ? errors() : ControllerUtils.get(this, property);
            if(newData != null) {
                Object oldData = getDataFromStore(property);
                if (oldData == null || (oldData != newData && !oldData.equals(newData))) {
                    for (PropertyChangeListener l : s)
                        l.propertyChange(new PropertyChangeEvent(this, property, oldData, newData));
                }
            }
        }
    }

    public void addSubscriber(String property, PropertyChangeListener l) {
        subscribers.addSubscriber(property, l);
    }

    private Object getDataFromStore(String property) {
        if(dataStore.containsKey(property)) {
            return dataStore.get(property);
        } else {
            return null;
        }

    }
}
