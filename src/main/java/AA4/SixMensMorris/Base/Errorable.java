package AA4.SixMensMorris.Base;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface Errorable {
    public String check();
    public String error() default "error in property %s";
    public Class<?> target() default ErrorUtils.class;
}
