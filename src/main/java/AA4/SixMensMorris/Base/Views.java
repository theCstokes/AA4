package AA4.SixMensMorris.Base;
import java.util.concurrent.Callable;
import java.util.function.Function;

import org.eclipse.swt.widgets.Composite;

import AA4.SixMensMorris.App;
import AA4.SixMensMorris.SetupPage.Controller.MainMenuPageControler;
import AA4.SixMensMorris.SetupPage.View.MainMenuPage;

public enum Views {
	MainView(MainMenuPageControler::new, MainMenuPage::new);
	
    private Callable<? extends Controller>  controlerCallable;
    private Function<? super Composite, ? extends Composite>  viewCallable;
    private Views(Callable<MainMenuPageControler> controlerCallable, Function<? super Composite, ? extends Composite> viewCallable) {
        this.controlerCallable = controlerCallable;
        this.viewCallable = viewCallable;
    }
    
    public void switchToPage() {
        try {
            ActivityManager.switchActivity(controlerCallable.call(), viewCallable.apply(App.shell));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
