package AA4.SixMensMorris.Base;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class ControllerUtils {
	 public static void set(Object obj, String property, Object o) {
	        try {
	            for(Method m : obj.getClass().getDeclaredMethods()) {
	                if(m.isAnnotationPresent(Set.class)) {
	                    Set s = m.getAnnotation(Set.class);
	                    if(s.prop().equals(property)) {
	                        m.setAccessible(true);
	                        m.invoke(obj, m.getParameterTypes()[0].cast(o));
	                    }
	                }
	            }
	        } catch (IllegalAccessException e) {
	            e.printStackTrace();
	        } catch (InvocationTargetException e) {
	            e.getCause().printStackTrace();
	        }
	    }

	    public static Object get(Object obj, String property) {
	        try {
	            for(Method m : obj.getClass().getDeclaredMethods()) {
	                if(m.isAnnotationPresent(Get.class)) {
	                    Get g = m.getAnnotation(Get.class);
	                    if(g.prop().equals(property)) {
	                        m.setAccessible(true);
	                        return m.invoke(obj);
	                    }
	                }
	            }
	        } catch (IllegalAccessException e) {
	            e.printStackTrace();
	        } catch (InvocationTargetException e) {
	            e.printStackTrace();
	        }
	        return null;
	    }
}
