package AA4.SixMensMorris.Base;

/**
 * @author aabramova
 * 
 */
public enum GameStates {
	Valid(true, "Game is valid" /*this is where the label outputs goes*/), Invalid(false, "Game is not valid" ),
	Mill(true, "Player got a mill" ) /*ect....*/;
	
	private boolean valid;
	private String data;
	private GameStates(boolean valid, String data) {
		this.valid = valid;
		this.data = data;
	}
	
	public boolean getValid() {
		return valid;
	}
	
	public String getData() {
		return data;
	}
	
	public static GameStates lookup(String data) {
		for(GameStates gs : values()) {
			if(gs.getData() == data) {
				return gs;
			}
		}
		return null;
	}
}
