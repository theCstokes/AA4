package AA4.SixMensMorris.Base;

import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.swt.widgets.Control;

/**
 * Enum data type that sets up the color constants 
 *
 */
public enum FontColor {
	GREEN(new RGB(81, 239, 92)), BLACK(new RGB(6, 6, 6)), RED(new RGB(230, 35, 35));
	
	private RGB color;
	/**
	 * Constructor assigns the color
	 * @param color - element color
	 */
	private FontColor(RGB color) {
		this.color = color;
	}
	
	/**
	 * Sets foreground and background colors on <b>item</b>
	 * @param parent - parent
	 * @param item - item
	 */
	public <T extends Control> void setColor(T item) {
		item.setForeground(new Color(item.getDisplay(), color));
		item.setBackground(item.getBackground());
	}
}
