package AA4.SixMensMorris.Base;

/**
 * Indexing locations on the frame
 */
public class PieceIndex {
	public int x;
	public int y;
	public PieceIndex(int x, int y) {
		this.x = x;
		this.y = y;
	}
}
