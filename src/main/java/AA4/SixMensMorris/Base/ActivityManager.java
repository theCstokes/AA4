package AA4.SixMensMorris.Base;

import org.eclipse.swt.custom.StackLayout;
import org.eclipse.swt.widgets.Composite;

import AA4.SixMensMorris.App;

/**
 * Created by cstokes on 01/03/16.
 */
public class ActivityManager {
	private static Controller currentControler;
	public static StackLayout layout;

	static {
		layout = new StackLayout();
	}	

	public static <Q extends Composite, T extends Controller<Q>> void switchActivity(T controler, Q c) {
		currentControler = controler;
		currentControler.setContentView(c);
		currentControler.onCreate();
		layout.topControl = c;
		App.shell.layout();
	}

	public static void setCurrentActivity(Controller a) {
		currentControler = a;
	}

	public static Controller getCurrentControler() {
		return currentControler;
	}
}
