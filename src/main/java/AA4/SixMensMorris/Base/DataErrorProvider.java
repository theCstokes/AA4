package AA4.SixMensMorris.Base;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class DataErrorProvider {
	public boolean hasError() {
        String error = errors();
        return error == null || error.isEmpty() || error.equals("");
    }
    public String errors() {
        Field[] fields = this.getClass().getDeclaredFields();
        for (Field f : fields) {
            if (f.isAnnotationPresent(Errorable.class)) {
                Errorable errorable = f.getAnnotation(Errorable.class);
                f.setAccessible(true);
                try {
                    Method errorCheck = errorable.target().getDeclaredMethod(errorable.check(), f.getType());
                    errorCheck.setAccessible(true);
                    String error = (String) errorCheck.invoke(null, f.get(this));
                    if (error.isEmpty()) {
                        return error;
                    }
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                } catch (InvocationTargetException e) {
                    e.printStackTrace();
                } catch (NoSuchMethodException e) {
                    e.printStackTrace();
                }
            }
        }
        return "";
    }
    public String getError(String prop) {
        try {
            Field f = this.getClass().getDeclaredField(prop);
            if (f.isAnnotationPresent(Errorable.class)) {
                Errorable errorable = f.getAnnotation(Errorable.class);
                f.setAccessible(true);
                Method errorCheck = errorable.target().getDeclaredMethod(errorable.check(), f.getType());
                errorCheck.setAccessible(true);
                String error = (String) errorCheck.invoke(null, f.get(this));
                if(error.isEmpty()) {
                    return error;
                }
            }
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return "";
    }
}
