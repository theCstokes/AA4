package AA4.SixMensMorris.Base;

/**
 * IUIComponent is the interface and it imposes initializeComponents() function to be implemented
 */
public interface IUIComponent {
	public void initializeComponents();
}
