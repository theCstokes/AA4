package AA4.SixMensMorris.Base;

import java.beans.PropertyChangeListener;

/**
 * Created by cstokes on 01/03/16.
 */
public abstract class AbstractCommand<T extends Controller<?>> extends NotifyPropertyChanged {
    public T controler;
    private PropertyChangeListener l = e -> {raisePropertyChanged("error");};

    public AbstractCommand(T controler) {
        this.controler = controler;

    }

    @Get(prop = "error")
    public abstract boolean canExecute();

    public abstract void execute();

    public void initSubscriptions(NotifyPropertyChanged npc, String... properties) {
        if(npc != null) {
            npc.addSubscriber("error", l);
            for (String p : properties) {
                npc.addSubscriber(p, l);
            }
        }
    }
}
