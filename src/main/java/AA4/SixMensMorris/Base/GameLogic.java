package AA4.SixMensMorris.Base;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * GameLogic.java contains the logic of the game
 */
public class GameLogic extends NotifyPropertyChanged {
	private Map<String, List<String>> map;
	static String[] basic = {"00","20","40","10","21","30","01","11","31","41","12","22","32","02","23","42"};
	static String[][] mill = {{"0","00","20","40"},{"0","10","21","30"},{"0","12","22","32"},{"0","02","23","42"},
					  {"0","00","01","02"},{"0","10","11","12"},{"0","30","31","32"},{"0","40","41","42"}};
	static String[][] connection = {{"00","20","01",""},{"20","00","40","21"},{"40","20","41",""},{"01","00","02","11"},
					  {"41","40","42","31"},{"02","01","23",""},{"23","02","42","22"},{"42","23","41",""},
					  {"10","21","11",""},{"21","20","10","30"},{"30","21","31",""},{"11","01","10","12"},
					  {"31","41","30","32"},{"12","11","22",""},{"22","23","12","32"},{"32","22","31",""}};
	
	private GameStates state = GameStates.Valid;
	
	/**
	 * Constructor setting required member variables
	 */
	public GameLogic(){
		map = new HashMap<>();
		for(int i=0; i<16; i++) map.put(basic[i], new ArrayList<String>());
	}
	
	/**
	 * Adds element to the hashmap to keep track of element positioning on the board
	 */
	public void addElementBoardState(int x, int y, String color){
		map.get(String.format("%d%d", x,y)).add(color);
		updateBoardState();
		raisePropertyChanged("state");
	}
	
	@Get(prop="state")
	public GameStates getBoardState() {
		return state;
	}
	
	/**
	 * checks the state of the board when Check Board button is pressed
	 * @return true if the board is valid, false otherwise
	 */
	private void updateBoardState(){
//		GameStates state = GameStates.Valid;
		int red = 0;
		int blue = 0;
		for(int i=0; i<basic.length; i++){
			 if(map.get(basic[i]).size() > 1){
				 for(int j=0; j<16; j++) map.get(basic[j]).clear();
				 state = GameStates.Invalid;
			 }
		}
		for (int i=0; i<basic.length; i++){ 
			if(map.get(basic[i]).contains("Blue")) blue++;
			else if(map.get(basic[i]).contains("Red")) red++;
		}
		state = (blue < 3 || red < 3) ? GameStates.Invalid : GameStates.Valid;
	}
	
	/**
	 * Evaluates if the move made by the user is allowable
	 * @return true if the move is allowed, false otherwise
	 */
	public boolean move(String from, String to, String color){
		if(map.get(to).size()!=0) return false; //if to is not empty
		for(int i=0; i<connection.length; i++){ //check if move is possible
			if(connection[i][0] == from){
				for(int j=1; j<4; j++){
					if(connection[i][j] == to){
						checkForMill("1", from); //if to is moved but was part of the mill, mill element 0 will be changed to 0
						map.get(to).add(color);
						if(checkForMill("0", to)) popElement(); //check for mill
						return true;
					}
				} 
			} 
		}
		return false;
	}
	
	/**
	 * Checks for mill combinations on the board
	 * @return true if there is a mill on the board, false otherwise
	 */
	public boolean checkForMill(String x, String element){
		for(int i=0; i<mill.length; i++){ 
			if(mill[i][0] == x){
				for(int j=1; j<4; j++){
					if(mill[i][j] == element){
						mill[i][0] = (x == "0") ? "1" : "0";
						return true;
						//NOT COMPLETED
					}
				} 
			} 
		}
		return false;
	}
	
	/**
	 * Pops the element that user will point to if the user got a mill
	 */
	public boolean popElement(){
		//TODO
		return true;
	}
}
