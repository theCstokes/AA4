package AA4.SixMensMorris.Game.Data;

import AA4.SixMensMorris.Base.PieceIndex;
import AA4.SixMensMorris.SetupPage.Data.User;

public class Players extends User {
	private boolean pieceSelected;
	private int indexSelected;
	public PieceIndex[] pieces;
	public Players(User user) {
		super(user);
		pieceSelected = false;
		pieces = new PieceIndex[Values.NUMBER_OF_PIECES];
	}
	
	public Players(User user, PieceIndex[] pieces) {
		super(user);
		pieceSelected = false;
		this.pieces = new PieceIndex[Values.NUMBER_OF_PIECES];
		for(int i = 0; i < Values.NUMBER_OF_PIECES; i++) { 
			this.pieces[i] = pieces[i];
		}
	}
	
	public boolean isPeiceSelected() {
		return pieceSelected;
	}
	
	public void setPeiceSelected(boolean peiceSelected) {
		this.pieceSelected = peiceSelected;
	}
	
	public int getIndexSelected() {
		return indexSelected;
	}
	
	public void setIndexSelected(int index) {
		this.indexSelected = index;
	}
	
}
