package AA4.SixMensMorris.Game.Data;

import AA4.SixMensMorris.Base.Get;
import AA4.SixMensMorris.Base.NotifyPropertyChanged;
import AA4.SixMensMorris.Base.Set;
import AA4.SixMensMorris.SetupPage.Data.User;

public class GameData extends NotifyPropertyChanged{
	User user1;
	User user2;
	User activeUser;
	
	public GameData(User user1, User user2) {
		this.user1 = user1;
		this.user2 = user2;
	}
	
	@Get(prop="activeUser")
	public User getActiveUser()  {
		return activeUser;
	}
	
	public User getUser1()  {
		return user1;
	}
	
	public User getUser2()  {
		return user2;
	}
	
	@Set(prop="activeUser")
	public void setActiveUser(String id) {
		if(id.equals("user1")) {
			activeUser = user1;
		} else if(id.equals("user2")) {
			activeUser = user2;
		}
	}
	
	public void setActiveUser(User user) {
		if(user.equals(user1) || user.equals(user2)) {
			activeUser = user;
		}
	}
	
	public User updateActiveUser() {
//		user1.setPeiceSelected(false);
//		user2.setPeiceSelected(false);
		if(activeUser == null) {
			activeUser = Math.random() > 0.5 ? user1 : user2;
		} else if(activeUser == user1) {
			activeUser = user2;
		} else {
			activeUser = user1;
		}
		return activeUser;
	}
}
