package AA4.SixMensMorris.Game.View;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.layout.RowData;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;

import AA4.SixMensMorris.Base.IUIComponent;

public class GamePage extends Composite implements IUIComponent {
	public ItemSelecter playerSelecter1;
//	public ItemSelecter playerSelecter2;
	public Board board;
	public Button actionButton;
	public Label errorLabel;
	
	public GamePage(Composite parent, int style, Object... args) {
		super(parent, SWT.NONE);
		initializeComponents();
	}

	@Override
	public void initializeComponents() {
		this.setLayout(new RowLayout());
		
		board = new Board(this);
		board.setLayoutData(new RowData(board.x, board.y));

		Group players = new Group(this, SWT.NONE);
		players.setLayout(new GridLayout(1, false));
		errorLabel = new Label(players, SWT.WRAP | SWT.LEFT);
		errorLabel.setLayoutData(new GridData(SWT.TOP, SWT.CENTER, true, false));
		playerSelecter1 = new ItemSelecter(players);
		playerSelecter1.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, true, true));
//		playerSelecter2 = new ItemSelecter(players);
//		playerSelecter2.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, true, true));
		actionButton = new Button(players, SWT.PUSH);
		actionButton.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, true, true));	
		actionButton.setText("Next Player Set Pieces");
	}
}
