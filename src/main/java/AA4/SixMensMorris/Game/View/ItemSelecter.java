package AA4.SixMensMorris.Game.View;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Listener;

import AA4.SixMensMorris.Base.IUIComponent;

public class ItemSelecter extends Composite implements IUIComponent {
	public String playerName;
	public Map<String, CircleButton> peices;
	private Group selecter;

	public ItemSelecter(Composite parent) {
		super(parent, SWT.NONE);
		peices = new HashMap<>();
		initializeComponents();
	}

	@Override
	public void initializeComponents() {
		this.setLayout(new FillLayout());
		selecter = new Group(this, SWT.NONE);
		GridLayout layout = new GridLayout(3, false);
		selecter.setLayout(layout);
		this.setLayoutData(new GridData(SWT.RIGHT, SWT.RIGHT, false, false));
	}

	public void addPeices(String id, int number, RGB rgb) {
		for (int i = 0; i < number; i++) {
			peices.put(id, new CircleButton(selecter, rgb, id, i));
		}
	}

	public void setTitle(String title) {
		selecter.setText(title);
	}
	
	public void setSelected(CircleButton c) {
		if(peices.containsValue(c)) {
			for (CircleButton p : peices.values()) {
				if(p.isSelected())
				p.setSelected(false);
			}
			c.setSelected(true);
		}
	}

	@Override
	public void addListener(int eventType, Listener listener) {
		for (CircleButton c : peices.values()) {
			c.addListener(eventType, e -> {
				setSelected(c);
				listener.handleEvent(e);
			});
		}
	}

}
