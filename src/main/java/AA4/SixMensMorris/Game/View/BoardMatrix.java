package AA4.SixMensMorris.Game.View;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.swt.widgets.Composite;

import AA4.SixMensMorris.Base.IUIComponent;
import AA4.SixMensMorris.Base.PieceIndex;

public class BoardMatrix implements IUIComponent{
	private final int size = 40;
	private int x;
	private int y;
	private int offset;
	private Map<Integer, List<BoardLocation>> data;
	private Composite parent;

	public BoardMatrix(Composite parent, int x, int y, int offest) {
		data = new LinkedHashMap<>();
		this.parent = parent;
		this.x = x;
		this.y = y;
		this.offset = offest;
		initializeComponents();
	}
	
	public void resetData() {
		for(Entry<Integer, List<BoardLocation>> entry : data.entrySet()) {
			for(BoardLocation bl : entry.getValue()) {
				bl.reset();
			}
		}
	}

	public boolean isItem(int target, int actual) {
		boolean ans = (target < actual && (target + size) > actual);
		return ans;
	}
	
	public BoardLocation getItem(PieceIndex loc) {
		int xIndex = 0; 
		for (Entry<Integer, List<BoardLocation>> entry : data.entrySet()) {
			if(xIndex == loc.x) {
				return entry.getValue().get(loc.y);
			}
			xIndex++;
		}
		return null;
	}
	
	public void setItem(int x, int y, RGB rgb) {
		int xIndex = 0; 
		for (Entry<Integer, List<BoardLocation>> entry : data.entrySet()) {
			if(xIndex == x) {
				entry.getValue().get(y).setColor(rgb);
			}
			xIndex++;
		}
	}
	
	public void setItem(PieceIndex loc, RGB rgb) {
		int xIndex = 0; 
		for (Entry<Integer, List<BoardLocation>> entry : data.entrySet()) {
			if(xIndex == loc.x) {
				entry.getValue().get(loc.y).setColor(rgb);
			}
			xIndex++;
		}
	}

	public PieceIndex getLocation(int x, int y) {
		int xCount = 0;
		int yCount = 0;
		for (Entry<Integer, List<BoardLocation>> entry : data.entrySet()) {
			if (isItem(entry.getKey(), y)) {
				for (BoardLocation loc : entry.getValue()) {
					if (isItem(loc.getX(), x)) {
						return new PieceIndex(xCount, yCount);
					}
					yCount++;
				}
			}
			xCount++;
		}
		return null;
	}

	@Override
	public void initializeComponents() {
		int[] ys = new int[5];
		int pos = 0;
		for (int j = offset; j < y + offset && pos < 5; j += y / 5) {
			ys[pos] = j - 8;
			pos++;
		}
		offset -= 8;
		addEven(ys[0], offset);
		addCenter(ys[1], offset);
		addMid(ys[2], offset);
		addCenter(ys[3], offset);
		addEven(ys[4], offset);
		
	}
	
	private void addEven(int y, int scip) {
		List<BoardLocation> items = new ArrayList<>();
		int count = 0;
		for (int i = scip; i < x + scip; i += x / 5) {
			if (count % 2 == 0) {
				System.out.println("L: " + i + ", " + y);
				items.add(new BoardLocation(parent, i, y));
			}
			count++;
		}
		data.put(y, items);
		System.out.println(data.size());
	}

	private void addCenter(int y, int scip) {
		List<BoardLocation> items = new ArrayList<>();
		int count = 0;
		for (int i = scip; i < x + scip; i += x / 5) {
			if (count > 0 && count < 4) {
				items.add(new BoardLocation(parent, i, y));
			}
			count++;
		}
		data.put(y, items);
	}

	private void addMid(int y, int scip) {
		List<BoardLocation> items = new ArrayList<>();
		int count = 0;
		for (int i = scip; i < x + scip && count < 5; i += x / 5) {
			if (count != 2) {
				items.add(new BoardLocation(parent, i, y));
			}
			count++;
		}
		data.put(y, items);
	}

	public void display(GC gc) {
		System.out.println(data.size());
		for (Entry<Integer, List<BoardLocation>> entry : data.entrySet()) {
			for (BoardLocation loc : entry.getValue()) {
				loc.redraw(gc);
			}

		}
	}
}
