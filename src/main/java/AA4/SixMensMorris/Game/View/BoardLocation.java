package AA4.SixMensMorris.Game.View;

import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.ImageData;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.swt.widgets.Composite;

import AA4.SixMensMorris.Base.IUIComponent;
import AA4.SixMensMorris.Resources.R;

/**
 * @author aabramova 
 * BoardLocation.java is the class which implements
 * IUIComponent. This class sets the Board location on the screen, gets
 * images of circle by using R.java and then fills this with color (red or blue).
 */
public class BoardLocation implements IUIComponent {
	private final int SIZE = 40;
	private final int DEFAULT_COLOR = ColorUtils.convetToHexCode(0, 0, 0);
	private ImageData data;
	private Image image;
	private int color;
	private int x;
	private int y;
	Composite parent;

	public BoardLocation(Composite parent, int x, int y) {
		this.parent = parent;
		this.x = x;
		this.y = y;
		data = new ImageData(R.CIRCLE);
		data = data.scaledTo(SIZE, SIZE);
		color = ColorUtils.convetToHexCode(0, 0, 0);
		initializeComponents();
	}

	public void setColor(RGB rgb) {
		int newColor = ColorUtils.convetToHexCode(rgb.red, rgb.green, rgb.blue);
		setColor(newColor);
	}

	private void setColor(int c) {
		for (int i = 0; i < SIZE; i++) {
			for (int j = 0; j < SIZE; j++) {
				if (data.getPixel(i, j) == color) {
					data.setPixel(i, j, c);
				}
			}
		}
		image = new Image(parent.getDisplay(), data);
		this.color = c;
		parent.redraw();
	}

	public void reset() {
		setColor(DEFAULT_COLOR);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see AA4.SixMensMorris.control.IUIComponent#initializeComponents()
	 */

	@Override
	public void initializeComponents() {
		for (int i = 0; i < SIZE; i++) {
			for (int j = 0; j < SIZE; j++) {
				if (data.getPixel(i, j) > ColorUtils.convetToHexCode(250, 250, 250)) {
					data.setPixel(i, j, color);
				}
			}
		}
		image = new Image(parent.getDisplay(), data);
	}

	public void redraw(GC gc) {
		gc.drawImage(image, x, y);
		System.out.println("draw");
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

}
