package AA4.SixMensMorris.Game.View;

public class ColorUtils {
	public static int convetToHexCode(int r, int g, int b) {
		int hex = 0;
		hex += r << 16;
		hex += g << 8;
		hex += b;
		return hex;
	}
}
