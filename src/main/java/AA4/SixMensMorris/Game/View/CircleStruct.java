package AA4.SixMensMorris.Game.View;

public class CircleStruct {
	private int[] data;

	public CircleStruct(int r, int offsetX, int offsetY) {
		data = new int[8 * r + 4];
		for (int i = 0; i < 2 * r + 1; i++) {
			int x = i - r;
			int y = (int) Math.sqrt(r * r - x * x);
			data[2 * i] = offsetX + x;
			data[2 * i + 1] = offsetY + y;
			data[8 * r - 2 * i - 2] = offsetX + x;
			data[8 * r - 2 * i - 1] = offsetY - y;
		}
	}

	public int[] getData() {
		return data;
	}
}
