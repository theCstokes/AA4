package AA4.SixMensMorris.Game.View;

import AA4.SixMensMorris.App;
import AA4.SixMensMorris.Base.AbstractCommand;
import AA4.SixMensMorris.SetupPage.Controller.BoardLayoutControler;

public class BoardValidCommand extends AbstractCommand<BoardLayoutControler> {
	
	/**
	 * Game start command
	 */
	public BoardValidCommand(BoardLayoutControler controler) {
		super(controler);
	}

	/**
	 * Executes the game
	 */
	@Override
	public boolean canExecute() {
		return true;
	}

	/**
	 * Starts new game
	 */
	@Override
	public void execute() {
		controler.view.errorLabel.setText(App.gameLogic.getBoardState().getData());
		controler.view.errorLabel.pack();
	}

}
