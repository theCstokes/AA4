package AA4.SixMensMorris.Game.View;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.ImageData;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;

import AA4.SixMensMorris.Base.IUIComponent;
import AA4.SixMensMorris.Base.PieceIndex;
import AA4.SixMensMorris.Resources.R;

public class Board extends Composite implements IUIComponent {
	public final int x;
	public final int y;
	private int offset = 0;
	public BoardMatrix data;

	public Board(Composite parent) {
		super(parent, SWT.NONE);
		Point p = this.getShell().getSize();
		int size = Math.min(p.x, p.y);
		x = size;
		y = size;
		buildBackground();
		data = new BoardMatrix(this, x, y, offset);
		initializeComponents();
	}

	@Override
	public void initializeComponents() {
		this.setLayout(new FillLayout());

		this.addPaintListener(e -> {
			System.out.println("printings");
			data.display(e.gc);
		});
		this.redraw();
	}

	private void buildBackground() {
		ImageData id = new ImageData(R.BOARD);
		id = id.scaledTo(x, y);
		boolean count = true;
		for (int i = 0; i < x; i++) {
			for (int j = 0; j < y; j++) {
				if (id.getAlpha(i, j) == 0) {
					Color c = this.getBackground();
					id.setPixel(i, j, ColorUtils.convetToHexCode(c.getRed(), c.getGreen(), c.getBlue()));
				} else {
					count = false;
				}
			}
			if (count)
				offset++;
		}
		Image image = new Image(this.getDisplay(), id);
		this.setBackgroundImage(image);
	}

	public void addPeiceListener(int type, Listener l) {
		this.addListener(type, e -> {
			PieceIndex index = data.getLocation(e.x, e.y);
			if (index != null) {
				BoardLocation loc = data.getItem(index);
				Event event = new Event();
				event.data = loc;
				event.x = index.x;
				event.y = index.y;
				l.handleEvent(event);
			}
		});
	}
}
