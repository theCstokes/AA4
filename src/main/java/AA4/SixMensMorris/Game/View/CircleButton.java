package AA4.SixMensMorris.Game.View;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.swt.graphics.Region;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Listener;

import AA4.SixMensMorris.Base.IUIComponent;

/**
 * @author aabramova
 * Class manages drawing and coloring circles on the board
 */
public class CircleButton extends Composite implements IUIComponent {
	private final int RADIOUS = 20;
	private final Color sel = new Color(this.getDisplay(), new RGB(192,192,192));
	private int alpha;
	private boolean s = false;
	private RGB rgb;
	private boolean visable;
	private int x = RADIOUS;
	private int y = RADIOUS;
	private Region region;
	private int index;
	private String id;

	public Canvas c;

	public CircleButton(Composite parent, RGB rgb, String id, int index) {
		super(parent, SWT.NONE);
		this.rgb = rgb;
		this.alpha = 255;
		visable = true;
		this.index = index;
		this.id = id;
		initializeComponents();
	}

//	public CircleButton(Composite parent, RGB rgb, int x, int y, int index) {
//		super(parent, SWT.NONE);
//		this.rgb = rgb;
//		this.alpha = 255;
//		this.x = x;
//		this.y = y;
//		this.index = index;
//		visable = true;
//		initializeComponents();
//	}

	public void setSelected(boolean selected) {
		s = selected;
		c.setBackground(selected ? sel : getColor());
	}

	public boolean isSelected() {
		return s;
	}

	public void setVisable(boolean visable) {
		this.visable = visable;
		c.setBackground(this.getBackground());
	}

	public boolean isVisable() {
		return visable;
	}

	public void setColor(RGB rgb) {
		this.rgb = rgb;
		this.setBackground(getColor());
	}

	@Override
	public void initializeComponents() {
		this.setLayout(new FillLayout());
		this.setBackground(new Color(this.getDisplay(), this.getBackground().getRGB(), 0));
		c = new Canvas(this, SWT.NONE);

		region = new Region();
		c.setBackground(getColor());
		region.add(new CircleStruct(RADIOUS, x, y).getData());
		c.setRegion(region);
		c.setSize(x, y);

	}
	
	public String getId() {
		return id;
	}

	private Color getColor() {
		return new Color(this.getDisplay(), rgb, alpha);
	}
	
	public int getIndex() {
		return index;
	}
	
	public void setIndex(int index) {
		this.index = index;
	}
	
	@Override
	public void addListener(int eventType, Listener listener) {
		c.addListener(eventType, e -> {
			e.widget = this;
			listener.handleEvent(e);
		});
	}
}
