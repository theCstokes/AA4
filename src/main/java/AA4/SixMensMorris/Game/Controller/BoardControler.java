package AA4.SixMensMorris.game.control;

import org.eclipse.swt.SWT;

import AA4.SixMensMorris.App;
import AA4.SixMensMorris.Base.Controller;
import AA4.SixMensMorris.Base.PieceIndex;
import AA4.SixMensMorris.Game.Data.Players;
import AA4.SixMensMorris.Game.View.Board;
import AA4.SixMensMorris.Game.View.BoardLocation;
import AA4.SixMensMorris.SetupPage.Data.User;

/**
 * BoardController.java is the class which extends Controller. 
 * This class lets the user choose one of its pieces (button) 
 * and loads the data of user remaining pieces.
 */

public class BoardControler extends Controller<Board> {
	private User activeUser;

	/**
	 * Constructor setting required member variables
	 */
	public BoardControler(Board board, User activeUser) {
		this.activeUser = activeUser;
		setContentView(board);
		setup();
	}
	
	@Override
	public void setup() {
		view.addPeiceListener(SWT.MouseDown, e -> {
			System.out.println("back");
			BoardLocation loc = (BoardLocation) e.data;
			if (loc != null && this.activeUser.isPeiceSelected()){
				loc.setColor(this.activeUser.getColor().getRGB());
				this.activeUser.pieces[this.activeUser.getIndexSelected()] = new PieceIndex(e.x, e.y);
//				this.activeUser.setPeiceSelected(false);
				App.gameLogic.addElementBoardState(""+e.x+e.y, this.activeUser.getColor().getName());
			}
		});		
	}

	/**
	 * Resets the data of the game board
	 */
	public void resetBoard() {
		view.data.resetData();
	}
	
	/**
	 * Sets active user
	 */
	public void setActiveUser(Players activeUser) {
		this.activeUser = activeUser;
	}
	
	/**
	 * Loads data on color and position of element to the board
	 */
	public void loadData(Players user1, Players user2) {
		for(PieceIndex i : user1.pieces) {
			if(i == null)
				continue;
			view.data.setItem(i, user1.getColor().getRGB());
		}
		for(PieceIndex i : user2.pieces) {
			if(i == null)
				continue;
			view.data.setItem(i, user2.getColor().getRGB());
		}
	}
}
