package AA4.SixMensMorris.SetupPage.Controller;

import AA4.SixMensMorris.App;
import AA4.SixMensMorris.Base.AbstractCommand;
import AA4.SixMensMorris.Base.GameStates;

/**
 * GameStartCommand.java is the class which implements ICommand.java. 
 * This class creates an object of MainMenuPageController.java class
 */

public class GameStartCommand extends AbstractCommand<MainMenuPageControler> {
	
	private boolean canExecute;
	
	/**
	 * Game start command
	 */
	public GameStartCommand(MainMenuPageControler controler) {
		super(controler);
		canExecute = false;
		App.gameLogic.addSubscriber("state", e -> {
			canExecute = (e.getNewValue() != GameStates.Invalid);
			controler.view.startButton.setEnabled(canExecute);
		});
	}

	/**
	 * Executes the game
	 */
	@Override
	public boolean canExecute() {
		System.out.println("cd = " + canExecute);
		return !controler.gameUsers.hasError() && canExecute;
	}

	/**
	 * Starts new game
	 */
	@Override
	public void execute() {
		System.out.println("run");
//		GameControler game = new GameControler(App.shell, controler.buildP1(), controler.buildP2());
	}

}
