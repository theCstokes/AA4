package AA4.SixMensMorris.SetupPage.Controller;

import AA4.SixMensMorris.Base.Bundle;
import AA4.SixMensMorris.Base.Controller;
import AA4.SixMensMorris.SetupPage.Data.GameUsers;
import AA4.SixMensMorris.SetupPage.Data.UserColors;
import AA4.SixMensMorris.SetupPage.View.MainMenuPage;

/**
 * MainMenuPageController.java is the class that extends Controler.java. This
 * class plays a vital role in this game since it creates a user by asking them
 * for their name, assigns them a color, provides an active label,adds listener
 * to the turn Button and manages each player data from the start.
 */
public class MainMenuPageControler extends Controller<MainMenuPage> {

	public BoardLayoutControler blc;
	public GameUsers gameUsers;

	/**
	 * Constructor setting required member variables
	 */
	public MainMenuPageControler() {
//		mainMenuPage = (MainMenuPage) Pages.MENU_PAGE.launch(parentComposite, SWT.NONE);
		gameUsers = new GameUsers(UserColors.BLUE, UserColors.RED);
	}
	
	@Override
	public void onCreate() {
//		UserPageControl upc1 = new UserPageControl(this, view.userSelector1, data.user1);
//		UserPageControl upc2 = new UserPageControl(this, view.userSelector2, data.user2);
//
//		blc = new BoardLayoutControler(this, view.layoutSelecter);
//		view.pack();

		bind("gameUsers.username1", view.userSelector1.userNameText);
		bind("gameUsers.username2", view.userSelector2.userNameText);
		bind("gameUsers.colorname1", view.userSelector1.colorSelector);
		bind("gameUsers.colorname1", view.userSelector2.colorSelector);
		
		Bundle b = new Bundle();
		b.store("view", view.layoutSelecter);
		b.store("gameUsers", gameUsers);
//		b.store("parent", this);
		
		BoardLayoutControler blc = new BoardLayoutControler(b);
		blc.onCreate();
		
//		DataBinding<Text, GameUsers> usernameMBinding1 = DataBinding.oneWayBinding(view.userSelector1.userNameText,
//				data::setUsername1);
//		DataBinding<Combo, GameUsers> colorMBinding1 = DataBinding.oneWayBinding(view.userSelector1.colorSelector,
//				data::setColor1);
//
//		DataBinding<Text, GameUsers> usernameMBinding2 = DataBinding.oneWayBinding(view.userSelector2.userNameText,
//				data::setUsername2);
//		DataBinding<Combo, GameUsers> colorMBinding2 = DataBinding.oneWayBinding(view.userSelector2.colorSelector,
//				data::setColor2);
//
//		DataBinding<Button, GameUsers> next = DataBinding.twoWayBinding(view.startButton, data, "error",
//				new GameStartCommand(this));
	}

//	public Players buildP1() {
//		User base = data.user1.getColor() == blc.getData().getUser1().getColor() ? data.user1 : data.user2;
//		Players p = new Players(base, blc.getData().getUser1().pieces);
//		return p;
//	}
//
//	public Players buildP2() {
//		User base = data.user1.getColor() == blc.getData().getUser1().getColor() ? data.user2 : data.user1;
//		Players p = new Players(base, blc.getData().getUser2().pieces);
//		return p;
//	}

}
