package AA4.SixMensMorris.SetupPage.Controller;

import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Text;


/**
 * UserPageControl.java is the class which extends Contoler.java. 
 * This class binds data to its associate user
 */

public class UserPageControl extends Controler<UserInfoPage, User> {

	/**
	 * Constructor binds data to associate user 
	 */
	public <T> UserPageControl(Controler<T, ?> parentControler, UserInfoPage v, User user) {
		super(parentControler, user);
		view = v;
		view.colorSelector.setItems(UserColors.getNames());
		setup();
	}

	@Override
	public void setup() {
		DataBinding<Text, User> usernameBinding1 = DataBinding.twoWayBinding(view.userNameText, data, "username");
		DataBinding<Combo, User> colorBinding1 = DataBinding.twoWayBinding(view.colorSelector, data, "color");
		data.addSubscriber("error", e -> {
			view.error.setText((String) e.getNewValue());
			if(DataFieldUtils.isNullEmptyOrWhiteSpace((String) e.getNewValue())) {
				FontColor.RED.setColor(view.error);
			}
			view.error.pack();
		});
	}
}
