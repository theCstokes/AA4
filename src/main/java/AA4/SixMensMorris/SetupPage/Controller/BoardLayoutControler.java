package AA4.SixMensMorris.SetupPage.Controller;

import AA4.SixMensMorris.Base.Bundle;
import AA4.SixMensMorris.Base.Controller;
import AA4.SixMensMorris.Game.Data.GameData;
import AA4.SixMensMorris.Game.View.BoardValidCommand;
import AA4.SixMensMorris.Game.View.GamePage;
import AA4.SixMensMorris.SetupPage.Data.GameUsers;
import AA4.SixMensMorris.SetupPage.Data.UserColors;
import AA4.SixMensMorris.SetupPage.View.MainMenuPage;

/**
 * @author aabramova
 * Front view of the game. Aligning instantiating elements on the board
 */
public class BoardLayoutControler extends Controller<GamePage>{
//	private GameUsers gameUsers;
	private GameData data;
	private BoardValidCommand gameStartCommand;
//	private CircleButton p2;
//	private CircleButton p1;
	
	public BoardLayoutControler(Bundle bundle) {
		setContentView((GamePage) bundle.get("view"));
		GameUsers gameUsers = (GameUsers) bundle.get("gameUsers");
		data = new GameData(gameUsers.user1, gameUsers.user2);
		view.playerSelecter1.addPeices("user1", 1, UserColors.BLUE.getRGB());
		view.playerSelecter1.addPeices("user2",1, UserColors.RED.getRGB());
		gameStartCommand = new BoardValidCommand(this);
	}

	@Override
	public void onCreate() {
//		bind("data.updateActiveUser", view.actionButton);
		bind("data.activeUser", view.playerSelecter1);
		bind("data.activeUser", view.board);
		bind("gameStartCommand", ((MainMenuPage) view.getParent()).checkStateButton);
	}

//	private void setUpPlayerSelecter(CircleButton p1, Players user1, CircleButton p2, Players user2) {
//		p1.addListener(SWT.MouseDown, e -> {
//			System.out.println(e.widget);
//			if(e.widget == p1) {
//				boolean sel = !p1.isSelected();
//				p2.setSelected(!sel && p2.isSelected());
//				p1.setSelected(sel);
//				if(sel) {
//					data.setActiveUser(user1);
//				}
//			}	
//	});
//	}

}
