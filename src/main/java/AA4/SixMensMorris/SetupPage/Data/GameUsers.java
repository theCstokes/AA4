package AA4.SixMensMorris.SetupPage.Data;

import AA4.SixMensMorris.Base.Errorable;
import AA4.SixMensMorris.Base.NotifyPropertyChanged;
import AA4.SixMensMorris.Base.Set;

/**
 * GameUsers.java is the class which extends NotifyPropertyChanged.java and 
 * implements IDataErrorProvider.java. This class provide the functionality 
 * to set the user name and their color which is then used by MainMenuPageController.jav
 */
public class GameUsers extends NotifyPropertyChanged {
	@Errorable(check = "isNullOrWhite")
	public User user1;
	@Errorable(check = "isNullOrWhite")
	public User user2;
	
	/**
	 * Constructor instantiating an object of User 1 and 2
	 */
	public GameUsers() {
		super();
		user1 = new User();
		user2 = new User();
	}
	
	public GameUsers(UserColors userColor1, UserColors userColor2) {
		super();
		user1 = new User(userColor1);
		user2 = new User(userColor2);
	}
	
	@Set(prop = "username1")
	public void setUsername1(String username) {
		user1.setUsername(username);
		raisePropertyChanged("user1");
	}

	@Set(prop = "color1")
	public void setColor1(String color) {
		user1.setColor(color);
		user2.setColor(UserColors.getUserColor(color).getOpposite().getName());
		raisePropertyChanged("user1");
	}

	@Set(prop = "username2")
	public void setUsername2(String username) {
		user2.setUsername(username);
		raisePropertyChanged("user2");
	}

	@Set(prop = "username2")
	public void setColor2(String color) {
		user2.setColor(color);
		user1.setColor(UserColors.getUserColor(color).getOpposite().getName());
		raisePropertyChanged("user1");
		raisePropertyChanged("user2");
	}
}
