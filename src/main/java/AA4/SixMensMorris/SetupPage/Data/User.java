package AA4.SixMensMorris.SetupPage.Data;

import AA4.SixMensMorris.Base.Errorable;
import AA4.SixMensMorris.Base.Get;
import AA4.SixMensMorris.Base.NotifyPropertyChanged;
import AA4.SixMensMorris.Base.Set;

public class User extends NotifyPropertyChanged {
	@Errorable(check = "isNullOrWhite")
	private String username;
	@Errorable(check = "isNullOrWhite")
	private UserColors color;

	public User() {
		super();
	}
	
	public User(UserColors color) {
		super();
		this.color = color;
	}
	
	public User(User user) {
//		super(user);
		this.username = user.username;
		this.color = user.color;
	}

	@Set(prop = "username")
	public void setUsername(String username) {
		this.username = username;
		raisePropertyChanged("username");
		raisePropertyChanged("error");
	}

	@Set(prop = "colorname")
	public void setColor(String color) {
		this.color = UserColors.getUserColor(color);
		raisePropertyChanged("color");
		raisePropertyChanged("error");
	}

	@Get(prop="username")
	public String getUsername() {
		return username;
	}

	@Get(prop="colorname")
	public String getColorName() {
		return color.getName();
	}
	
	@Get(prop="color")
	public UserColors getColor() {
		return color;
	}

}
