package AA4.SixMensMorris.SetupPage.Data;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Device;
import org.eclipse.swt.graphics.RGB;

public enum UserColors {
	BLUE("Blue", new RGB(0,0,250), SWT.COLOR_BLUE), RED("Red", new RGB(250,0,0), SWT.COLOR_RED);
	
	private String name;
	private RGB rgb;
	private int color;
	
	private UserColors(String name, RGB rgb, int color) {
		this.name = name;
		this.rgb = rgb;
		this.color = color;
	}
	
	public String getName() {
		return name;
	}
	
	public Color getColor(Device d) {
		return new Color(d, rgb);
	}
	
	public UserColors getOpposite() {
		if(this == BLUE) {
			return UserColors.RED;
		} else {
			return BLUE;
		}
	}
	
	public int getColor() {
		return color;
	}
	
	public RGB getRGB() {
		return rgb;
	}
	
	public static UserColors getUserColor(String name) {
		for(UserColors uc : values()) {
			if(uc.getName().equals(name)) {
				return uc;
			}
		}
		return null;
	}
	
	public static String[] getNames() {
		int length = values().length;
		String[] names = new String[length];
		for(int i = 0; i < length; i++) {
			names[i] = values()[i].getName();
		}
		return names;
	}
}
