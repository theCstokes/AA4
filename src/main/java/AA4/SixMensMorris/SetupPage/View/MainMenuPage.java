package AA4.SixMensMorris.SetupPage.View;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;

import AA4.SixMensMorris.Base.IUIComponent;
import AA4.SixMensMorris.Game.View.GamePage;

/**
 * MainMenuPage initializing main page components
 */
public class MainMenuPage extends Composite implements IUIComponent {
	
	public UserInfoPage userSelector1;
	public UserInfoPage userSelector2;
	public Label error;
	public GamePage layoutSelecter;
	public Button checkStateButton;
	public Button startButton;
	
	/**
	 * Constructor that calls super constructor and initializeComponents()
	 * @param parent 
	 * @param style
	 * @param args
	 */
	public MainMenuPage(Composite parent) {
		super(parent, SWT.NONE);
		initializeComponents();
	}

	/**
	 * Initializes buttons, error field, page layout on the main menu page
	 */
	@Override
	public void initializeComponents() {
		GridLayout layout = new GridLayout(1, false);
		this.setLayout(layout);
		
		error = new Label(this, SWT.NONE);
		
		Group g = new Group(this, SWT.SHADOW_IN);
		g.setLayout(new GridLayout(2, false));
		g.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		g.setText("Users");
		
		userSelector1 = new UserInfoPage(g);
		userSelector2 = new UserInfoPage(g);
		
		layoutSelecter = new GamePage(this, SWT.None);
		layoutSelecter.setLayoutData(new GridData(SWT.BOTTOM, SWT.BOTTOM, false, false));
		
		Composite l = new Composite(this, SWT.NONE);
		l.setLayout(new GridLayout(2, false));
		
		checkStateButton = new Button(l, SWT.PUSH);
		checkStateButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.CENTER, false, false));
		checkStateButton.setText("Check Board");
		
		startButton = new Button(l, SWT.PUSH);
		startButton.setLayoutData(new GridData(SWT.RIGHT, SWT.BOTTOM, false, true));
		startButton.setText("Start");
	}

}
