package AA4.SixMensMorris.SetupPage.View;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import AA4.SixMensMorris.Base.FontColor;
import AA4.SixMensMorris.Base.IUIComponent;

/**
 * UserInfoPage.java is the class which extends Composite and implements 
 * IUIComponent.java. This class hold UI representation of the user, 
 * information like his name and color
 */
public class UserInfoPage extends Composite implements IUIComponent {

	private Label userNameLabel;
	private Label userColor;
	public Label error;
	public Text userNameText;
	public Combo colorSelector;
	
	/**
	 * Constructor that calls super constructor and initializeComponents() function
	 * @param parent
	 */
	public UserInfoPage(Composite parent) {
		super(parent, SWT.NONE);
		initializeComponents();
	}

	/**
	 * Initializes components such as labels, text
	 */
	@Override
	public void initializeComponents() {
		this.setLayout(new GridLayout());
		this.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));	
		
		error = new Label(this, SWT.NONE | SWT.READ_ONLY);
		error.setLayoutData(new GridData(SWT.BEGINNING, SWT.CENTER, false, false));
		error.setText("Enter username data");
		FontColor.GREEN.setColor(error);
		
		Group userGroup = new Group(this, SWT.None);
		GridLayout layout = new GridLayout(2, false);
		userGroup.setLayout(layout);
		
		userGroup.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		userGroup.setText("User 1");
		
		userNameLabel = new Label(userGroup, SWT.NONE | SWT.READ_ONLY);
		userNameLabel.setLayoutData(new GridData(SWT.BEGINNING, SWT.CENTER, false, false));
		userNameLabel.setText("Username: ");
		
		userNameText = new Text(userGroup, SWT.SINGLE | SWT.LEAD | SWT.BORDER);
		userNameText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		
		userNameLabel = new Label(userGroup, SWT.NONE | SWT.READ_ONLY);
		userNameLabel.setLayoutData(new GridData(SWT.BEGINNING, SWT.CENTER, false, false));
		userNameLabel.setText("Colour: ");
		
		colorSelector = new Combo(userGroup, SWT.DROP_DOWN | SWT.BORDER | SWT.READ_ONLY);
		
	}

}
