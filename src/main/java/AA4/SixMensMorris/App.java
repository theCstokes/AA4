package AA4.SixMensMorris;

import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

import AA4.SixMensMorris.Base.ActivityManager;
import AA4.SixMensMorris.Base.GameLogic;
import AA4.SixMensMorris.Base.Views;

public class App 
{
	public static String test;
	public static Shell shell;
	public static GameLogic gameLogic;
	
	public static void main(String[] args) {
		gameLogic = new GameLogic();
		Display display = new Display();
		shell = new Shell(display);
		Color c = shell.getBackground();
		
		shell.setLayout(ActivityManager.layout);
		Views.MainView.switchToPage();
//		MainMenuPageControler mpc = new MainMenuPageControler(shell);

		shell.open();
		shell.setMaximized(true);
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch())
				display.sleep();
		}
		display.dispose();
	}
}
